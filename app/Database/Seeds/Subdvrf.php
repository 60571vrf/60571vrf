<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Subdvrf extends Seeder
{
    public function run()
    {
        $data = [
            'full_name'=> 'Jon Doe',
        ];
        $this->db->table('author')->insert($data);
        $data = [
            'full_name'=> 'Jon Doe1',
        ];
        $this->db->table('author')->insert($data);
        $data = [
            'full_name'=> 'Jon Doe2',
        ];
        $this->db->table('author')->insert($data);


        $data = [
            'j_name'=> 'Наука',
        ];
        $this->db->table('journal')->insert($data);
        $data = [
            'j_name'=> 'Атом',
        ];
        $this->db->table('journal')->insert($data);
        $data = [
            'j_name'=> 'Галактика',
        ];
        $this->db->table('journal')->insert($data);


        $data = [

            'id_journal'=> 1,
            'id_author'=> 1,
            'name' => 'публикация100',
            'date' => '2021-01-30',
            'photo' => 'https://www.flaticon.com/svg/vstatic/svg/4064/4064205.svg?token=exp=1615145621~hmac=afc5741a92fa25e3a4a7a809570e2706',
            'description' => 'Тут должен быть текст публикации',
        ];
        $this->db->table('publications')->insert($data);
        $data = [
            'id_journal'=> 2,
            'id_author'=> 2,
            'name' => 'публикация10',
            'date' => '2021-01-30',
            'photo' => 'https://www.flaticon.com/svg/vstatic/svg/4064/4064205.svg?token=exp=1615145621~hmac=afc5741a92fa25e3a4a7a809570e2706',
            'description' => 'Тут должен быть текст публикации',
        ];
        $this->db->table('publications')->insert($data);
        $data = [
            'id_journal'=> 3,
            'id_author'=> 3,
            'name' => 'публикация2',
            'date' => '2021-01-30',
            'photo' => 'https://www.flaticon.com/svg/vstatic/svg/4064/4064205.svg?token=exp=1615145621~hmac=afc5741a92fa25e3a4a7a809570e2706',
            'description' => 'Тут должен быть текст публикации',
        ];
        $this->db->table('publications')->insert($data);

    }
}