<?php


namespace App\Models;

use CodeIgniter\Model;

class JournalModel extends Model
{
    protected $table = 'journal'; //таблица, связанная с моделью
    //Перечень задействованных в модели полей таблицы
    protected $allowedFields = ['j_name'];

    public function getJournal($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }
}