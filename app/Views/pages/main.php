<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<?php if (! $ionAuth->loggedIn()): ?>
    <div class="jumbotron text-center">
        <img class="mb-4" src="https://snipstock.com/assets/cdn/png/f02ad9db9ca20fc4898249037af040f3.png" alt="" width="72" height="72"><h1 class="display-4">PScience</h1>
        <p class="lead">Это приложение поможет узнать всю информацию о научных публикациях</p>
        <a class="btn btn-primary btn-lg" href="auth/login" role="button">Войти</a>
    </div>
<?php else: ?>
    <div class="jumbotron text-center">
        <img class="mb-4" src="https://snipstock.com/assets/cdn/png/f02ad9db9ca20fc4898249037af040f3.png" alt="" width="72" height="72"><h1 class="display-4">PScience</h1>
        <p class="lead">Это приложение поможет узнать всю информацию о научных публикациях</p>
        <a class="btn btn-primary btn-lg" href="auth/logout" role="button">Выйти</a>
    </div>
<?php endif ?>
<?= $this->endSection() ?>