<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <?php use CodeIgniter\I18n\Time; ?>
        <?php if (!empty($publications)) : ?>
<!--        --><?php //var_dump($publications)?>
            <div class="card mb-13" style="max-width: 1200px;">
                <div class="row">
                    <div class="col-md-2 d-flex align-items-center">
                        <?php if(is_null($publications['picture_url'])): ?>
                            <img class="w-100" src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fpromdevelop.ru%2Fwp-content%2Fuploads%2Fmirnyj-atom.jpg&f=1&nofb=1" alt="">
                        <?php else : ?>
                            <img src="<?= esc($publications['picture_url']); ?>" class="card-img w-100" alt="<?= esc($publications['name']); ?>">
                        <?php endif; ?>
                    </div>
                    <div class="col-md-10">
                        <div class="card-body">
                            <h5 class="card-title"><?= esc($publications['name']); ?></h5>
                            <div class="my-0">Дата публикации:</div>
                            <p class="card-text"><?= esc($publications['date']); ?></p>
                          <!--   <div class="d-flex justify-content-between">
                                <div class="my-0">Порядок публикации:</div>
                                <div class="text-muted"><?= esc($publications['id']); ?> </div>
                            </div> -->
                            <div class="d-flex justify-content-">
                                <div class="my-0">Наименование журнала:</div>
                                <div class="card-text"><?= esc($publications['j_name']); ?> </div>
                            </div>
                            <div class="d-flex justify-content-">
                                <div class="my-0">Имя автора:</div>
                                <div class="card-text"><?= esc($publications['full_name']); ?> </div>
                            </div>
                            <div class="my-0">Текст публикации:</div>
                            <h5 class="card-text"><?= esc($publications['description']); ?></h5>
                        </div>
                    </div>
                    <div class="card-body float-md-right">
                        <a href="<?= base_url()?>/publications/edit/<?= esc($publications['id']); ?>" class="btn btn-primary mt-4">Редактировать</a>
                        <a href="<?= base_url()?>/publications/delete/<?= esc($publications['id']); ?>" class="btn btn-primary mt-4">Удалить</a>
                    </div>
                </div>
            </div>
        <?php else : ?>
            <p>Публикация не найдена.</p>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>