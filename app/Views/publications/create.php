<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container" style="max-width: 540px;">

        <?= form_open_multipart('publications/store'); ?>
        <div class="form-group">
            <label for="name">Наименование публикации</label>
            <input type="text" class="form-control <?= ($validation->hasError('name')) ? 'is-invalid' : ''; ?>"
                   name="name"
                   value="<?= old('name'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('name') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="id_journal">Журнал:</label>
            <select class="form-control <?= ($validation->hasError('id_journal')) ? 'is-invalid' : ''; ?>"
                    name='id_journal'
                    onChange="" id="id_journal">
                <option value="-1">Выберите Журнал</option>
                <?php foreach ($journal as $item): ?>
                    <option value=<?= esc($item['id_j']); ?>>
                        <?= esc($item['j_name']); ?> </option>
                <?php endforeach; ?>
            </select>
            <div class="invalid-feedback">
                <?= $validation->getError('id_journal') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="id_author">Автор:</label>
            <select class="form-control <?= ($validation->hasError('id_author')) ? 'is-invalid' : ''; ?>"
                    name='id_author'
                    onChange="" id="id_author">
                <option value="-1">Выберите автора</option>
                <?php foreach ($author as $item): ?>
                    <option value=<?= esc($item['id_a']); ?>><?= esc($item['full_name']); ?> </option>
                <?php endforeach; ?>
            </select>
            <div class="invalid-feedback">
                <?= $validation->getError('id_author') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="birthday">Дата публикации</label>
            <input type="date" class="form-control <?= ($validation->hasError('date')) ? 'is-invalid' : ''; ?>"
                   name="date" value="<?= old('date'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('date') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="description">Текст публикации</label>
            <input type="text" class="form-control <?= ($validation->hasError('description')) ? 'is-invalid' : ''; ?>"
                   id="description" name="description" value="<?= old('description'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('description') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="birthday">Изображение</label>
            <input type="file" class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>" name="picture">
            <div class="invalid-feedback">
                <?= $validation->getError('picture') ?>
            </div>
        </div>
        <div class="form-group" style="margin-top: 20px">
            <button type="submit" class="btn btn-primary" name="submit">Создать</button>
        </div>
        </form>
    </div>
<?= $this->endSection() ?>