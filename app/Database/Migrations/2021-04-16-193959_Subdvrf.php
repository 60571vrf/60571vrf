<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Subdvrf extends Migration
{
    public function up()
    {   // author
        if (!$this->db->tableexists('author'))
        {
            // Setup Keys
            $this->forge->addkey('id_a', TRUE);

            $this->forge->addfield(array(
                'id_a' => array('type' => 'INT', 'null' => FALSE, 'auto_increment' => TRUE),
                'full_name' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
            ));
            // create table
            $this->forge->createtable('author', TRUE);
        }
        // journal
        if (!$this->db->tableexists('journal'))
        {
            // Setup Keys
            $this->forge->addkey('id_j', TRUE);

            $this->forge->addfield(array(
                'id_j' => array('type' => 'INT', 'null' => FALSE, 'auto_increment' => TRUE),
                'j_name' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
            ));
            // create table
            $this->forge->createtable('journal', TRUE);
        }
        // publications
        if (!$this->db->tableexists('publications'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'null' => FALSE, 'auto_increment' => TRUE),
                'id_journal' => array('type' => 'INT', 'null' => TRUE),
                'id_author' => array('type' => 'INT', 'null' => TRUE),
                'name' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
                'date' => array('type' => 'DATE', 'null' => TRUE),
                'photo' => array('type' => 'CHAR', 'constraint' => '255', 'null' => TRUE),
                'description' => array('type' => 'TEXT',  'null' => FALSE),
            ));
            $this->forge->addForeignKey('id_journal','journal','id_j','RESTRICT','RESTRICT');
            $this->forge->addForeignKey('id_author','author','id_a','RESTRICT','RESTRICT');
            // create table
            $this->forge->createtable('publications', TRUE);
        }
    }

    //--------------------------------------------------------------------

    public function down()
    {
        $this->forge->droptable('author');
        $this->forge->droptable('journal');
        $this->forge->droptable('publications');
    }
}
