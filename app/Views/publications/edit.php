<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

    <div class="container" style="max-width: 540px;">

        <?= form_open_multipart('publications/update'); ?>
        <input type="hidden" name="id" value="<?= $publications["id"] ?>">

        <div class="form-group">
            <label for="name">Наименование публикации</label>
            <input type="text" class="form-control <?= ($validation->hasError('name')) ? 'is-invalid' : ''; ?>"
                   name="name"
                   value="<?= $publications["name"]; ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('name') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="id_journal">Журнал:</label>
            <select class="form-control <?= ($validation->hasError('id_journal')) ? 'is-invalid' : ''; ?>"
                    name='id_journal'
                    onChange="" id="id_journal">
                <option value="-1">Выберите Журнал</option>
                <?php foreach ($journal as $item): ?>
                    <option value="<?= ($item['id_j']) ?>" <?php if ($publications["id_journal"] == $item['id_j']) echo "selected"; ?>>
                        <?= esc($item['j_name']); ?>
                    </option>
                <?php endforeach; ?>
            </select>
            <div class="invalid-feedback">
                <?= $validation->getError('id_journal') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="id_author">Автор:</label>
            <select class="form-control <?= ($validation->hasError('id_author')) ? 'is-invalid' : ''; ?>"
                    name="id_author"
                    onChange="" id="id_author">
                <option value="-1">Выберите Автора</option>
                <?php foreach ($author as $item): ?>
                    <option value="<?= ($item['id_a']) ?>" <?php if ($publications["id_author"] == $item['id_a']) echo "selected"; ?>>
                        <?= esc($item['full_name']); ?>
                    </option>
                <?php endforeach; ?>
            </select>
            <div class="invalid-feedback">
                <?= $validation->getError('id_author') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="birthday">Дата публикации</label>
            <input type="date" class="form-control <?= ($validation->hasError('date')) ? 'is-invalid' : ''; ?>"
                   name="date" value="<?= $publications["date"] ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('date') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="description">Текст публикации</label>
            <input type="text" class="form-control <?= ($validation->hasError('description')) ? 'is-invalid' : ''; ?>"
                   id="description" name="description" value="<?= $publications["description"] ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('description') ?>
            </div>
        </div>
        <div class="form-group">
            <label for="birthday">Изображение</label>
            <input type="file" value=" <?= $publications["picture_url"] ?> " class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>" name="picture">
            <div class="invalid-feedback">
                <?= $validation->getError('picture') ?>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Сохранить</button>
        </div>
        </form>
    </div>
<?= $this->endSection() ?>