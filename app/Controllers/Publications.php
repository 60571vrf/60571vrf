<?php namespace App\Controllers;

use App\Models\AuthorModel;
use App\Models\JournalModel;
use App\Models\PublicationsModel;
use Aws\S3\S3Client;
use CodeIgniter\Model;

class Publications extends BaseController
{
    public function index() //Обображение всех записей
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new PublicationsModel();
        $data ['publications'] = $model->getPublications();
        $data ['author'] = $model->getAuthors();
        echo view('publications/view_all', $this->withIon($data));
    }

    public function view($id = null) //отображение одной записи
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new PublicationsModel();
//        $data ['publications'] = $model->getInfoWithPublications($id);
        $data ['publications'] = $model->getPublications($id);
        echo view('publications/view', $this->withIon($data));
    }

    public function create()
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        $model = new AuthorModel();
        $data ['author'] = $model->getAuthor();
        $model = new JournalModel();
        $data ['journal'] = $model->getJournal();
        echo view('publications/create', $this->withIon($data));
    }

    public function store()
    {
        helper(['form','url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'name' => 'required|min_length[3]|max_length[255]',
                'date'  => 'required',
                'id_journal'  => 'required|greater_than[-1]',
                'id_author'  => 'required|greater_than[-1]',
                'description'  => 'required',
                'picture'  => 'is_image[picture]|max_size[picture,1024]'
            ])) {
            $insert = null;
            //получение загруженного файла из HTTP-запроса
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);

            }
            {
                $model = new PublicationsModel();

                $data= [
                    'name' => $this->request->getPost('name'),
                    'date' => $this->request->getPost('date'),
                    'id_journal' => $this->request->getPost('id_journal'),
                    'id_author' => $this->request->getPost('id_author'),
                    'description' => $this->request->getPost('description'),
                ];
                if (!is_null($insert)){
                    $data['picture_url'] = $insert['ObjectURL'];
                }
                $model->save($data);
                session()->setFlashdata('message', lang('60571vrf.publications_create_success'));
                return redirect()->to('/publications');
            }
        }
        else
        {
            return redirect()->to('/publications/create')->withInput();
        }
    }
    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new PublicationsModel();

        helper(['form']);
        $data ['publications'] = $model->getPublications($id);
        $data ['author'] = $model->getAuthors($id);
        $data ['validation'] = \Config\Services::validation();
        $model = new AuthorModel();
        $data ['author'] = $model->getAuthor();
        $model = new JournalModel();
        $data ['journal'] = $model->getJournal();
        echo view('publications/edit', $this->withIon($data));
    }
    public function update()
    {
        helper(['form','url']);
        echo '/publications/edit/'.$this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id'  => 'required',
                'name' => 'required|min_length[3]|max_length[255]',
                'date'  => 'required',
                'id_journal'  => 'required|greater_than[-1]',
                'id_author'  => 'required|greater_than[-1]',
                'description'  => 'required',
                'picture'  => 'is_image[picture]|max_size[picture,1024]'
            ]))
        {
            $insert = null;
            //получение загруженного файла из HTTP-запроса
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);

            }
            $model = new PublicationsModel();
            $data= [
                'id' => $this->request->getPost('id'),
                'name' => $this->request->getPost('name'),
                'date' => $this->request->getPost('date'),
                'id_journal' => $this->request->getPost('id_journal'),
                'id_author' => $this->request->getPost('id_author'),
                'description' => $this->request->getPost('description'),
            ];
            if (!is_null($insert)){
                $data['picture_url'] = $insert['ObjectURL'];
            }
            $model->save($data);
            //session()->setFlashdata('message', lang('Curating.rating_update_success'));

            return redirect()->to('/publications');
        }
        else
        {
            return redirect()->to('/publications/edit/'.$this->request->getPost('id'))->withInput();
        }
    }
    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new PublicationsModel();
        $model->delete($id);
        return redirect()->to('/publications');
    }
    public function viewAllWithPublications()
    {
        if ($this->ionAuth->isAdmin())
        {
            //Подготовка значения количества элементов выводимых на одной странице
            if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
            {
                //сохранение кол-ва страниц в переменной сессии
                session()->setFlashdata('per_page', $this->request->getPost('per_page'));
                $per_page = $this->request->getPost('per_page');
            }
            else {
                $per_page = session()->getFlashdata('per_page');
                session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
                if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
            }
            $data['per_page'] = $per_page;
            //Обработка запроса на поиск
            if (!is_null($this->request->getPost('search')))
            {
                session()->setFlashdata('search', $this->request->getPost('search'));
                $search = $this->request->getPost('search');
            }
            else {
                $search = session()->getFlashdata('search');
                session()->setFlashdata('search', $search);
                if (is_null($search)) $search = '';
            }
            $data['search'] = $search;
            helper(['form','url']);
            $model = new PublicationsModel();
            $data['publications'] = $model->getInfoWithPublications(null, $search)->paginate($per_page, 'group1');
            $data['pager'] = $model->pager;
            echo view('publications/view_all_with_publications', $this->withIon($data));
        }
        else
        {
                session()->setFlashdata('message', lang('admin_permission_needed'));
            return redirect()->to('/auth/login');
        }
    }
}

