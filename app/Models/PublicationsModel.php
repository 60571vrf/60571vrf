<?php namespace App\Models;

use CodeIgniter\Model;

class PublicationsModel extends Model
{
    protected $table = 'publications'; //таблица, связанная с моделью
    protected $allowedFields = ['name', 'date', 'id_journal', 'id_author', 'description', 'picture_url'];

    public function getPublications($id = null)
    {
        $builder = $this->select('*')
            ->join('journal', 'journal.id_j = publications.id_journal')
            ->join('author', 'author.id_a = publications.id_author');
        if (!is_null($id)) {
            return $builder->where(['publications.id' => $id])->first();
        }
        return $builder->findAll();
    }
    public function getAuthors($id = null)
    {
        $builder = $this->select('*')
            ->join('author', 'author.id_a = publications.id_author');
        if (!is_null($id)) {
            return $builder->where(['publications.id' => $id])->first();
        }
        return $builder->findAll();
    }

    public function getInfoWithPublications($id = null, $search = '')
    {
        $builder = $this->select('*')
            ->join('journal', 'journal.id_j = publications.id_journal')
            ->join('author', 'author.id_a = publications.id_author')
            ->like("name", $search, 'both', null, true)
            ->orlike("full_name", $search, 'both', null, true)
            ->orlike("j_name", $search, 'both', null, true);

        if (!is_null($id)) {
            return $builder->where(['publications.id' => $id])->first();
        }
        return $builder;
    }
}