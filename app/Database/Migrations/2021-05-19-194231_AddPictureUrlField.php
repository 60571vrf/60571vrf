<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddPictureUrlField extends Migration
{

    public function up()
    {
        if ($this->db->tableexists('publications')) {
            $this->forge->addColumn('publications', array(
                'picture_url' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE)
            ));
        }
    }
    public function down()
    {
        $this->forge->dropColumn('publications', 'picture_url');
    }
}
