<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <h2>Все публикации</h2>

        <?php if (!empty($publications) && is_array($publications)) : ?>

            <?php foreach ($publications as $item): ?>

                <div class="card mb-3" style="max-width: 540px;">
                    <div class="row">
                        <div class="col-md-4 d-flex align-items-center">
                            <?php if(is_null($item['picture_url'])): ?>
                                <img class="w-100" src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fpromdevelop.ru%2Fwp-content%2Fuploads%2Fmirnyj-atom.jpg&f=1&nofb=1" alt="">
                            <?php else : ?>
                                <img height="150" src="<?= esc($item['picture_url']); ?>" class="card-img w-100" alt="<?= esc($item['name']); ?>">
                            <?php endif; ?>
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title"><?= esc($item['name']); ?></h5>
                                <p class="card-text"><?= esc($item['date']); ?></p>
                                <a href="<?= base_url()?>/publications/view/<?= esc($item['id']); ?>" class="btn btn-primary">Просмотреть</a>
                            </div>
                        </div>
                    </div>
                </div>

            <?php endforeach; ?>
        <?php else : ?>
            <p>Невозможно найти публикации.</p>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>