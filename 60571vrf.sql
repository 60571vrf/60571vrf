-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Фев 28 2021 г., 19:54
-- Версия сервера: 8.0.23-0ubuntu0.20.04.1
-- Версия PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `60571vrf`
--

-- --------------------------------------------------------

--
-- Структура таблицы `автор`
--

CREATE TABLE `автор` (
  `id` int NOT NULL COMMENT 'id автора',
  `id_публикации` int NOT NULL COMMENT 'id публикации',
  `id_персоны` int NOT NULL COMMENT 'id персоны',
  `доля_участия` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `автор`
--

INSERT INTO `автор` (`id`, `id_публикации`, `id_персоны`, `доля_участия`) VALUES
(1, 3, 1, 101),
(2, 5, 1, 50),
(3, 5, 3, 50),
(4, 7, 3, 100),
(5, 8, 8, 25),
(6, 8, 11, 75),
(7, 9, 2, 100),
(8, 10, 9, 100),
(9, 11, 4, 100),
(10, 12, 10, 100),
(11, 13, 6, 100),
(12, 14, 7, 100),
(13, 15, 11, 33),
(14, 16, 4, 100),
(15, 17, 4, 100),
(16, 18, 5, 100),
(17, 19, 6, 100),
(18, 20, 7, 50),
(19, 21, 1, 100),
(20, 6, 5, 50),
(21, 4, 6, 50),
(22, 3, 11, 50),
(23, 6, 1, 50),
(24, 4, 8, 50),
(25, 3, 7, 50),
(26, 20, 4, 50),
(27, 15, 2, 33),
(28, 15, 9, 33);

-- --------------------------------------------------------

--
-- Структура таблицы `журнал`
--

CREATE TABLE `журнал` (
  `id` int NOT NULL COMMENT 'id',
  `наименование` varchar(255) NOT NULL COMMENT 'наименование журнала'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `журнал`
--

INSERT INTO `журнал` (`id`, `наименование`) VALUES
(1, 'Атом'),
(2, 'Тесла'),
(3, 'Лекциум'),
(4, 'Галактика');

-- --------------------------------------------------------

--
-- Структура таблицы `персона`
--

CREATE TABLE `персона` (
  `id` int NOT NULL COMMENT 'id персоны',
  `ФИО` varchar(255) NOT NULL COMMENT 'ФИО',
  `дата_рождения` date DEFAULT NULL COMMENT 'дата рождения'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `персона`
--

INSERT INTO `персона` (`id`, `ФИО`, `дата_рождения`) VALUES
(1, 'Александр А.А.', '1990-10-02'),
(2, 'Иванов И.И.', '1965-12-01'),
(3, 'Дмитриев Д.Д.', '1978-02-11'),
(4, 'Игорев С.А.', NULL),
(5, 'Андреев М.Т.', NULL),
(6, 'Семченко В.Т.', '2000-08-10'),
(7, 'Шишкин И.П.', NULL),
(8, 'Волков П.Т.', NULL),
(9, 'Игнатьев Ф.С.', '1960-02-28'),
(10, 'Ратушев А.А.', '1995-05-11'),
(11, 'Дербенев И.П.', '1976-02-02');

-- --------------------------------------------------------

--
-- Структура таблицы `публикация`
--

CREATE TABLE `публикация` (
  `id` int NOT NULL COMMENT 'id',
  `id_журнала` int NOT NULL COMMENT 'id журнала',
  `наименование` varchar(255) NOT NULL COMMENT 'наименование публикации',
  `дата_публикации` date NOT NULL COMMENT 'дата публикации'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Дамп данных таблицы `публикация`
--

INSERT INTO `публикация` (`id`, `id_журнала`, `наименование`, `дата_публикации`) VALUES
(3, 1, 'Ядерная физика', '2021-02-02'),
(4, 2, 'Электроакустика', '2020-03-17'),
(5, 3, 'Популярные транзисторы', '2010-03-11'),
(6, 4, 'Солнечная система', '1999-01-25'),
(7, 1, 'Публикация4', '2019-01-01'),
(8, 4, 'Публикация5', '2009-01-01'),
(9, 2, 'Публикация6', '2015-05-11'),
(10, 3, 'Публикация7', '1945-08-11'),
(11, 1, 'Публикация8', '1978-08-11'),
(12, 2, 'Публикация9', '1995-03-11'),
(13, 4, 'Публикация10', '1996-08-11'),
(14, 1, 'Публикация11', '1986-08-11'),
(15, 1, 'Публикация12', '1986-08-11'),
(16, 2, 'Публикация13', '1987-08-11'),
(17, 2, 'Публикация14', '1988-08-11'),
(18, 4, 'Публикация15', '1989-08-11'),
(19, 1, 'Публикация16', '1977-08-11'),
(20, 2, 'Публикация17', '1978-08-11'),
(21, 3, 'Публикация18', '1988-08-11');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `автор`
--
ALTER TABLE `автор`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_персоны` (`id_персоны`),
  ADD KEY `id_публикации` (`id_публикации`);

--
-- Индексы таблицы `журнал`
--
ALTER TABLE `журнал`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `персона`
--
ALTER TABLE `персона`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `публикация`
--
ALTER TABLE `публикация`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_журнала` (`id_журнала`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `автор`
--
ALTER TABLE `автор`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'id автора', AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT для таблицы `журнал`
--
ALTER TABLE `журнал`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'id', AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `персона`
--
ALTER TABLE `персона`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'id персоны', AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `публикация`
--
ALTER TABLE `публикация`
  MODIFY `id` int NOT NULL AUTO_INCREMENT COMMENT 'id', AUTO_INCREMENT=22;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `автор`
--
ALTER TABLE `автор`
  ADD CONSTRAINT `автор_ibfk_1` FOREIGN KEY (`id_персоны`) REFERENCES `персона` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `автор_ibfk_2` FOREIGN KEY (`id_публикации`) REFERENCES `публикация` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Ограничения внешнего ключа таблицы `публикация`
--
ALTER TABLE `публикация`
  ADD CONSTRAINT `публикация_ibfk_1` FOREIGN KEY (`id_журнала`) REFERENCES `журнал` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
