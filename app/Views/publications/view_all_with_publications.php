<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main">
        <?php if (!empty($publications) && is_array($publications)) : ?>
            <h2>Все публикации:</h2>
            <div class="d-flex justify-content-between mb-2">
                <?= $pager->links('group1','my_page') ?>
                <?= form_open('publications/viewAllWithPublications', ['style' => 'display: flex']); ?>
                <select name="per_page" class="ml-3" aria-label="per_page">
                    <option value="2" <?php if($per_page == '2') echo("selected"); ?>>2</option>
                    <option value="5"  <?php if($per_page == '5') echo("selected"); ?>>5</option>
                    <option value="10" <?php if($per_page == '10') echo("selected"); ?>>10</option>
                    <option value="20" <?php if($per_page == '20') echo("selected"); ?>>20</option>
                </select>
                <button class="btn btn-outline-success" type="submit" >На странице</button>
                </form>
                <?= form_open('publications/viewAllWithPublications',['style' => 'display: flex']); ?>
                <input type="text" class="form-control ml-3" name="search" placeholder="Имя или описание" aria-label="Search"
                       value="<?= $search; ?>">
                <button class="btn btn-outline-success" type="submit">Найти</button>
                </form>
            </div>
            <table class="table table-striped">
                <thead>
                <th scope="col">Дата публикации</th>
                <th scope="col">Имя публикации</th>
                <th scope="col">Наименовение журнала</th>
                <th scope="col">Автор публикации</th>
                <th scope="col">Управление</th>

                </thead>
                <tbody>
                <?php foreach ($publications as $item): ?>
                    <tr>
                        <td><?= esc($item['date']); ?>
                        </td>
                        <td><?= esc($item['name']); ?></td>
                        <td><?= esc($item['j_name']); ?></td>
                        <td><?= esc($item['full_name']); ?></td>
                        <td>
                            <a href="<?= base_url()?>/publications/view/<?= esc($item['id']); ?>" class="btn btn-primary btn-sm">Просмотреть</a>
                            <a href="<?= base_url()?>/publications/edit/<?= esc($item['id']); ?>" class="btn btn-warning btn-sm">Редактировать</a>
                            <a href="<?= base_url()?>/publications/delete/<?= esc($item['id']); ?>" class="btn btn-danger btn-sm">Удалить</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

        <?php else : ?>
            <div class="text-center">
                <p>Публикации не найдены </p>
                <a class="btn btn-primary btn-lg" href="<?= base_url()?>/publications/create"><span class="fas fa-tachometer-alt" style="color:white"></span>&nbsp;&nbsp;Создать публикацию</a>
            </div>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>